package com.tutorial.main;

import java.awt.Graphics;
import java.util.LinkedList;

public class Handler {

	LinkedList <GameObject> object =  new LinkedList<GameObject> ();  
	
	
	public  void tick(){
		for(int x = 0; x < object.size(); x++){
			GameObject objeto = object.get(x);
			objeto.tick();
		}
	}
	
	public  void render(Graphics g){
		for(int x = 0; x < object.size(); x++){
			GameObject objeto = object.get(x);
			objeto.render(g);
		}
	}
	
	public void addObject(GameObject objeto){
		object.addLast(objeto);
	}
	
}
